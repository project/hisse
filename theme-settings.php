<?php

  use Drupal\Core\Form\FormStateInterface;





function hisse_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['social_media'] = [
    '#type' => 'details',
    '#title' => t('Social Media Settings'),
    '#open' => TRUE,
  ];

  $form['social_media']['all_icons_show'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Show social icons in footer'),
    '#default_value' => theme_get_setting('all_icons_show', 'hisse'),
    '#description'   => t("Check this option to show social icons in footer. Uncheck to hide."),
  ];


  $form['social_media']['links'] = [
    '#type' => 'textarea',
    '#title' => t('Social Media Links'),
    '#description' => t('Enter each link on a new line.'),
    '#default_value' => theme_get_setting('links'),
    '#states' => [
      'visible' => [
        ':input[name="all_icons_show"]' => ['checked' => TRUE],
      ],
    ],
 ];


  $form['slideshow'] = [
    '#type' => 'details',
    '#title' => t('Front page slideshow.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['slideshow']['slide_count'] = [
    '#type' => 'number',
    '#title' => t('Number of slides'),
    '#default_value' => theme_get_setting('slide_count'),
    '#description'  => t("Enter the number of slides required & Save configuration."),
    '#markup' => '<div class="messages messages--warning">Clear caches after making any changes in theme settings. <a href="../../config/development/performance">Click here to clear cache</a></div>',
  ];

  $form['slideshow']['slide'] = [
    '#markup' => t('Change the banner image, title, description and link using below fieldset.'),
  ];

  for ($i = 1; $i <= theme_get_setting('slide_count'); $i++) {
    $form['slideshow']['slide' . $i] = [
      '#type' => 'details',
      '#title' => t('Slide ' . $i),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['slideshow']['slide' . $i]['slide_image' . $i] = [
      '#type' => 'managed_file',
      '#title' => t('Slide ' . $i . ' Image'),
      '#default_value' => theme_get_setting('slide_image' . $i, 'hisse'),
      '#upload_location' => 'public://',
    ];
    $form['slideshow']['slide' . $i]['slide_text_' . $i] = [
      '#type' => 'textarea',
      '#title' => t('Slide ' . $i . ' Text'),
      '#default_value' => theme_get_setting('slide_text_' . $i, 'hisse'),
    ];
  }
}



