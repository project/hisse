(function ($, Drupal, drupalSettings, once) {
    'use strict';
    Drupal.behaviors.scrollToTop = {
        attach: function (context, settings) {
            $('.uk-animation-slide-top').hide();
            $(window).on('scroll', function () {                
                var scrollTop = $(this).scrollTop();
                var docHeight = $(document).height();
                var winHeight = $(window).height();
                var scrollPercent = (scrollTop / (docHeight - winHeight)) * 100;
                if (scrollPercent >= 20) {
                  $('.uk-animation-slide-top').show();
                }else{
                    $('.uk-animation-slide-top').hide();
                }
              });
        }
    };
})(jQuery, Drupal, drupalSettings, once);
